<?php
date_default_timezone_set('America/Denver'); // TODO: kludge, set system timezone in php.ini

require(realpath(__DIR__ . '/../vendor/autoload.php'));

use Google\Cloud\Vision\VisionClient;

$projectId = 'image-recog';

$vision = new VisionClient([
    'projectId' => $projectId
]);

// - retrieve the formdata
// - process

$image = $vision->image(fopen($_FILES['file']['tmp_name'], 'r'), [
    'faces',          // Corresponds to `FACE_DETECTION`
    'landmarks',      // Corresponds to `LANDMARK_DETECTION`
    'logos',          // Corresponds to `LOGO_DETECTION`
    'labels',         // Corresponds to `LABEL_DETECTION`
    'text',           // Corresponds to `TEXT_DETECTION`,
    'document',       // Corresponds to `DOCUMENT_TEXT_DETECTION`
    'safeSearch',     // Corresponds to `SAFE_SEARCH_DETECTION`
    'imageProperties',// Corresponds to `IMAGE_PROPERTIES`
    'crop',           // Corresponds to `CROP_HINTS`
    'web'             // Corresponds to `WEB_DETECTION`
]);

echo json_encode($vision->annotate($image)->info());
