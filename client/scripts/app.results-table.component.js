let ResultsTableComponent = {
    templateUrl: '../tpl/app.results-table.tpl.html',
    bindings: {
        results: '<',
    },
}

export default ResultsTableComponent;

