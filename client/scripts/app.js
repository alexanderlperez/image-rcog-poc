'use strict';

///////////////////////////////////
//  ES6-related/Webpack-related  //
///////////////////////////////////

if (module.hot) {
  module.hot.accept();
}

import 'babel-polyfill';

///////////////////
//  App-related  //
///////////////////

import 'bootstrap';

import angular from 'angular';
import angularResource from 'angular-resource';
import 'angular-spinner';

import AppController from './app.controller.js';
import FileUpload from './app.file-upload.directive.js';
import ResultsTableComponent from './app.results-table.component.js';
import ImageResultsService from './app.image-results.service.js';
import ISOLANGS from './app.language-codes.constant.js';

/////////////////
//  Resources  //
/////////////////

import '../styles/app.scss';

////////////////
//  App core  //
////////////////

angular
    .module('app', ['angularSpinner'])
    .constant('ISOLANGS', ISOLANGS)
    .controller('AppController', AppController)
    .directive('fileUpload', FileUpload)
    .component('resultsTable', ResultsTableComponent)
    .service('ImageResultsService', ImageResultsService)
    
    
