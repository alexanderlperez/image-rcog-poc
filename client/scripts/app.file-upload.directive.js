class FileUpload {
    constructor() {
        this.restrict = 'A';
        this.scope = true;
        this.controller = FileUploadController;
    }

    link(scope, elem, attr, ctrl) {
        elem.bind('change', function () {
            var data = new FormData();
            data.append('file', elem[0].files[0]);

            ctrl.sendFile(data,function (res) {
                ctrl.updateResults(res.data);
            });
        })
    }

    static directiveFactory() {
        return new FileUpload();
    }
}

class FileUploadController {
    constructor($http, ImageResultsService, ISOLANGS, usSpinnerService) {
        this.$http = $http;
        this.ImageResultsService = ImageResultsService;
        this.ISOLANGS = ISOLANGS;
        this.usSpinnerService = usSpinnerService;
    }

    sendFile(data, callback) {
        this.usSpinnerService.spin('spinner-1');

        this.$http({
            url: 'app/process_img.php',
            method: 'POST',
            data: data,
            headers: { 'Content-Type': undefined }
        }).then(function (res) {
            callback(res);

            this.usSpinnerService.stop('spinner-1');
        }.bind(this))
    }

    updateResults(results) {
        this.ImageResultsService.results.splice(0)
        this.ImageResultsService.results.push(this.processResults(results));
    }

    processResults(results) {
        // fields 
        // - fullTextAnnotation.text
        // - fullTextAnnotation.pages[0].property.detectedLanguages[0].languageCode
        // - imagePropertiesAnnotation.dominantColors.colors[0].color
        // - labelAnnotations[].description, ...score
        // - webDetection.fullMatchingImages[].url
        // - webDetection.pagesWithMatchingImages[].url
        // - visuallySimilarImages[].url

        var processedAnnotations = results.labelAnnotations.map(function (antn) {
            return {
                description: antn.description,
                score: antn.score,
            };
        });

        var processedMatchingImages = results.webDetection.fullMatchingImages.map(function (img) {
            return img.url;
        });

        var processedMatchingPages = results.webDetection.pagesWithMatchingImages.map(function (page) {
            return page.url;
        });

        var processedSimilarImages = results.webDetection.visuallySimilarImages.map(function (img) {
            return img.url;
        });

        var processedColor = [
            results.imagePropertiesAnnotation.dominantColors.colors[0].color.red,
            results.imagePropertiesAnnotation.dominantColors.colors[0].color.green,
            results.imagePropertiesAnnotation.dominantColors.colors[0].color.blue,
        ];

        var processedLang = this.ISOLANGS[results.fullTextAnnotation.pages[0].property.detectedLanguages[0].languageCode].name;

        var data = {
            text: results.fullTextAnnotation.text,
            language: processedLang,
            dominantColor: processedColor,
            annotations: processedAnnotations,
            matchingImages: processedMatchingImages,
            matchingPages: processedMatchingPages,
            similarImages: processedSimilarImages,
        };

        return data;
    }
}

FileUploadController.$inject = [ '$http', 'ImageResultsService', 'ISOLANGS', 'usSpinnerService' ];

// directiveFactory is conventional for keeping angular 
// callers clean of `class`-related initialization boilerplate
export default FileUpload.directiveFactory; 
