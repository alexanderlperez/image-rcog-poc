class AppController {
    constructor(ImageResultsService) {
        this.results = ImageResultsService.results;
    }
}

AppController.$inject = [ 'ImageResultsService' ];
export default AppController;
